<?php 
    /* Get All Terms */
    $terms = get_terms('category');
    $data  = array();

    /* Create jSon Data For Autocomplete */
    if ( $terms ) { $x = 0;
        foreach ( $terms as $term ) { $x++;
            // Title 
            $data[$x]['value'] = $term->term_id;
            $data[$x]['label'] = $term->name;

            // Description 
            $x = $x+1;
            $data[$x]['value'] = $term->term_id;
            $data[$x]['label'] = $term->description;
        }
    }

    /* Fetch Data with GA metrics */
    if ( isset($_GET['s']) || isset($_GET['s_l']) ) {

        /* Create Query For Search Post By Term ID */

        /* Query By TERM ID */
        if ( $_GET['s'] ) {
            $query = "
            SELECT
                   p.ID, p.post_title, p.post_name, p.post_content, p.post_password, p.post_date, p.post_type 
            FROM
                    $wpdb->posts p
                 LEFT JOIN 
                    $wpdb->term_relationships AS tr ON (p.ID = tr.object_id)
                 LEFT JOIN 
                    $wpdb->term_taxonomy AS tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                 WHERE
                    p.post_status = 'publish' 
                    AND p.post_type = 'post'
                    AND tt.taxonomy = 'category'  
                    AND tt.term_id = '".$_GET['s']."'
                    AND DATE(p.post_date) >= '".$rangeFrom."'
                    AND DATE(p.post_date) <= '".$rangeTo."'
            ";
        } 

        $_result = $wpdb->get_results($query,ARRAY_A);

        /* Fetch API data using $_result */
        $filterArray = array();
        $api_data_array = array();

        if ( is_array($_result)  ) { $x = 0;
            foreach($_result as $currentPost){ $x++;   
                $api_data_array[$x]['post_date'] = get_post_time( get_option( 'date_format' ), false, $currentPost, true );
                $api_data_array[$x]['post_title'] = $currentPost['post_title'];
                $api_data_array[$x]['post_link'] = get_permalink($currentPost['ID']);

                try{
                    $optParams = array(
                            'dimensions' => 'ga:pageTitle,ga:pagePath,ga:hostname',
                            'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',      
                            'filters' => 'ga:pagePath=~/'.$currentPost['post_name'],
                            'max-results' => '1000');

                    $gaResults = $analytics->data_ga->get(
                            'ga:'.wbStatsGAViewId,
                            $rangeFrom,
                            $rangeTo,
                            'ga:sessions',
                            $optParams);
                    
                    if ( $gaResults ) {
                        $api_data_array[$x]['ga'] = $gaResults;
                    }
                } catch (Exception $ex) {
                    echo '<!-- $ex is '.print_r($ex, true).' -->';
                }
            }
        }
    }
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-12">  
            <form method="get" action="/wp-admin/admin.php">    
            	<input type="hidden" name="page"  value="wb-vs-google-analytics" />
                <input type="hidden" name="display" value="videoCatKey" />   

                <h4>Select the date range for your report</h4>

               	From: <input type="date" id="rangeFrom" name="rangeFrom"
				       value="<?php echo $rangeFrom; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
                To: <input type="date" id="rangeTo" name="rangeTo"
				       value="<?php echo $rangeTo; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
                <h4>Search Category By Keyword</h4>
                
                <input type="text"  id="s" name="s_l" value="" autocomplete="on" required="required">
                <input type="hidden" id="catid" name="s" value="" />

                

                <button class="btn btn-primary wb-stat-buttons" id="search-video">Search Videos</button>

                <br>
                <div class="alert alert-danger wb-alert-box" style="margin-top:30px; display:none;">
                    <strong>Search Invalid</strong> Please select a keyword from the suggestions
                </div> 
            </form>            
        </div>
    </div>

    <br />
    <?php if ( isset($_GET['s']) || isset($_GET['s_l']) ) : ?>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <h3>Search results for “<?= $_GET['s_l'] ?>”</h3>                
                <table class="table table-striped table-bordered table-hover" id="gaResults">
                    <thead>
                    <tr>
                        <th class="text-center">Date Published</th>
                        <th class="text-center">Post Title</th>
                        <th class="text-center">Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The total number of pages viewed."></span></th>
                        <th class="text-center">Unique Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The pageviews that are generated by the same user during the same session."></span></th>
                        <th class="text-center">Average Time On Page</th>
                    </tr>
                    </thead>                
                    
                    <?php if ( count( $api_data_array ) > 0 ) : ?>
                        <?php foreach ( $api_data_array as $post_data ) : ?>
                            <tr>
                                <td class="text-center"> <?= $post_data['post_date'] ?> </td>
                                <td class="text-center"> 
                                    <a href="<?= $post_data['post_link'] ?>" target="_blank">
                                        <?= $post_data['post_title'] ?>
                                    </a> 
                                </td>
                                <td class="text-center"> 
                                    <?= trim($post_data['ga']->totalsForAllResults['ga:pageviews']) > 0 ?
                                    $post_data['ga']->totalsForAllResults['ga:pageviews'] : '-' ?> 
                                </td>
                                <td class="text-center"> 
                                    <?= trim($post_data['ga']->totalsForAllResults['ga:uniquePageviews']) > 0 ?
                                    $post_data['ga']->totalsForAllResults['ga:uniquePageviews'] : '-' ?> 
                                </td>
                                <td class="text-center"> 
                                    <?= trim($post_data['ga']->totalsForAllResults['ga:avgTimeOnPage']) > 0 ?
                                    (sec2hms(($post_data['ga']->totalsForAllResults['ga:avgTimeOnPage']/$post_data['ga']->totalsForAllResults['ga:pageviews'])))
                                     : '-' ?> 
                                </td>
                            </tr>   
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tbody>
                            <tr><td style="border-right: 0px none;">There are no videos to display.</td>
                                <td style="border-left: 0px none; border-right: 0px none;"></td>
                                <td style="border-left: 0px none; border-right: 0px none;"></td>
                                <td style="border-left: 0px none; border-right: 0px none;"></td>
                                <td style="border-left: 0px none;"></td>
                            </tr>
                        </tbody>
                    <?php endif;?>
       
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>


<script>
  jQuery(document).ready(function($){
    var availableCat = <?= json_encode(array_values($data)); ?>;
    $( "#s" ).on( "keydown", function( event ) {
        if( !$(this).val() ) {
            $('#catid').val("");
        }
      }).autocomplete({
      source: availableCat,
      select: function( event, ui ) {
        $( "#s" ).val( ui.item.label );
        $( "#catid" ).val( ui.item.value );
        return false;
      }
    });

    $('#search-video').click(function(e){
        if ( !$('#catid').val() ) {
            $('.alert-danger').show();
            e.preventDefault();
        }
    });

  });
</script>

<style>
  .ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
    max-width:400px;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
</style>