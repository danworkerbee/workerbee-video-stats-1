<?php
/**
 * Plugin Name: Workerbee Video Reports
 * Plugin URI: http://workerbee.tv
 * Description: This plugin will allow users to view stats for each video. Stats will be taken from both brightcove and google analytics
 * Version: 1.57
 * Author: WorkerBee.TV
 * Author URI: http://workerbee.tv
 */

//define plugin path
define( 'wbStatsPATH', plugin_dir_path(__FILE__) );
error_reporting(0);

global $wpdb;
global $wb_stats, $wb_stats_options, $wb_ent_options;
$wb_stats_options = get_option('wb_stats_options');
$wb_stats_options['bcAccessToken'] = '';
$wb_ent_options = get_option('wb_ent_options');

 
function wb_stats_display_media() {

}

if (!class_exists("Workerbee_Video_Stats")) {
   class Workerbee_Video_Stats {
      var $admin_options_name = "WBVideoStatsOptions";

      function Workerbee_Video_Stats() {//constructor
         //add_action( 'wp_enqueue_scripts', array( $this, 'wb_scripts' ) );
      }
      
      function init() {
         global $wpdb;
         global $wb_stats, $wb_stats_options, $wb_ent_options;         
      
      }      
      
      

      
      function wb_scripts(){
         /*
         wp_register_style( 'wbBootstrapStylesheet', plugins_url('css/bootstrap.min.css', __FILE__) );
         wp_register_style( 'wbBootstrapTheme', plugins_url('css/bootstrap-theme.min.css', __FILE__) );         
         wp_register_script( 'wbBootstrapJs', plugins_url('js/bootstrap.min.js', __FILE__) );
         wp_register_script( 'wbStatsJs', plugins_url('js/wb-stats-scripts.js', __FILE__) );
         wp_register_script( 'wbStatsMultiSelectJs', plugins_url('js/multiselect/js/jquery.multi-select.js', __FILE__) );
         //wp_register_script( 'wbStatsMaxLength', plugins_url('js/jquery.maxlength.js', __FILE__) );
         
         wp_register_style( 'jquery-ui-style', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css' );
         wp_register_style( 'jquery-ui-dialog', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js' );
         wp_register_style( 'wbStatsMultiSelectStyle', plugins_url('js/multiselect/css/multi-select.css', __FILE__) );
         
                          
         wp_enqueue_style( 'wbBootstrapStylesheet');
         wp_enqueue_style( 'wbBootstrapTheme');
         wp_enqueue_script( 'wbBootstrapJs');
         wp_enqueue_script( 'wbStatsJs');
         //wp_enqueue_script( 'wbStatsMaxLength');
         
         wp_enqueue_style( 'wbStatsMultiSelectStyle');
         wp_enqueue_script( 'wbStatsMultiSelectJs');
         
         wp_enqueue_script( 'jquery-ui-core');
         wp_enqueue_style( 'jquery-ui-style');         
         wp_enqueue_script( 'jquery' );
         wp_enqueue_script( 'jquery-ui-dialog' );
          * 
          */
                  
      }      
      
      
      function displayDashboard(){
         require_once('pages/dashboard.php');
      }

      
      function displayGA(){
        require_once('pages/gaReport.php');
      }
      
      function displayListPage(){
         /*
         $display = $_GET['display'];
         switch ($display){
            case 'form':
               require_once('pages/listAdd.php');
               break;
            case 'edit':
               require_once('pages/listEdit.php');
               break;
            default:
               require_once('pages/displayList.php');      
         }
           */       
      }           
      
      function wb_stats_add_meta_box(){
          /*
         global $wpdb;
         $mediaList = $wpdb->get_results( 
            "
               SELECT *
               FROM wb_stats_dmedia
               WHERE status = 1
            " );
         
         if( count($mediaList) > 0 ){                     
            add_meta_box( 'wb-stats-metabox' , 'Video Reports', array($this, 'display_meta_box'), 'post', 'normal', 'high', $mediaList );
         }         
           * 
           */
      }
      
      function display_meta_box($mediaList){
          /*
         global $post, $wb_stats_options;             
?>
    <link rel='stylesheet' id='wbStatsMultiSelectStyle-css'  href='/wp-content/plugins/workerbee-video-stats/js/multiselect/css/multi-select.css' type='text/css' media='all' />
    <script type='text/javascript' src='/wp-content/plugins/workerbee-video-stats/js/multiselect/js/jquery.multi-select.js'></script>
   <script type="text/javascript">
      function cDelAddDMediaToPost(checkElement){
         jQuery(function ($) {
            var dMediaId = parseInt($(checkElement).attr('id').split('wb_stats_dmediaId')[1]);
            if( $(checkElement).attr('checked') && $(checkElement).attr('checked') == 'checked' && $('#wb_stats_dmediapost'+dMediaId).length > 0 ){
               $('#wb_stats_dmediapost'+dMediaId+'_div').show();
               $('#wb_stats_dmediapost'+dMediaId+'_div textarea').show();
            }
            else if( !$(checkElement).attr('checked') || $(checkElement).attr('checked') != 'checked' ){
               $('#wb_stats_dmediapost'+dMediaId+'_div').hide();
               $('#wb_stats_dmediapost'+dMediaId+'_div textarea').hide();
            }            
         });
      }
      function wbCdelToggleDisplay(){                  
         jQuery(function ($) {            
            if( $('#wbCdelEnableCheckbox').is(':checked') || $('#wbCdelEnableCheckbox:checked').length > 0 ){
               $('.wbCdelShowForm').show();               
               $('#wb_stats_listIds').multiSelect();
               
               if( $('#wb_stats_listIds').val() == null ){
                   $('#wb_stats_listIds').val([<?php echo $wb_stats_options['defaultList']; ?>]);
                   $('#wb_stats_listIds').multiSelect('refresh');
               }
               //$('.wbCdelShowForm input[type="checkbox"]').attr('checked', 'checked');
               //$('.wbCdelShowForm textarea[id^="wb_stats_dmediapost"], .wbCdelShowForm div[id^="wb_stats_dmediapost"]').show();
            }
            else{
               $('.wbCdelShowForm').hide();
               $('.wbCdelShowForm input[type="checkbox"]').removeAttr('checked');
            }
         });
      }      
      function wbCdelSelectAll(){
         jQuery(function ($) {  
            $('.wbCdelShowForm input[type="checkbox"]').attr('checked', 'checked');
            $('.wbCdelShowForm textarea[id^="wb_stats_dmediapost"], .wbCdelShowForm div[id^="wb_stats_dmediapost"]').show();
            $('#wbCdelDeselectAllCheckbox').show();
            $('#wbCdelSelectAllCheckbox').hide();                           
         });         
      }
      function wbCdelDeselectAll(){
         jQuery(function ($) {  
            $('.wbCdelShowForm textarea[id^="wb_stats_dmediapost"], .wbCdelShowForm div[id^="wb_stats_dmediapost"]').hide();
            $('.wbCdelShowForm input[type="checkbox"]').removeAttr('checked');      
            $('#wbCdelDeselectAllCheckbox').hide();
            $('#wbCdelSelectAllCheckbox').show();         
         });         
      }      
      
      
      
   </script>
   <style>
      .wbCdelShowForm{
         display: none;
      }
   </style>
   <input type="checkbox" id="wbCdelEnableCheckbox" name="wbCdelEnableCheckbox" onchange="wbCdelToggleDisplay();"/>
   <label for="wbCdelEnableCheckbox">Enable notifications</label>
   <p class="wbCdelShowForm">Select which media to send out notifications for when the post has been published.</p>
   <div class="wbCdelShowForm" style="margin-left: 30px; margin-bottom: 30px;">
      <input type="button" class="btn btn-xs btn-primary" id="wbCdelSelectAllCheckbox" name="wbCdelSelectAllCheckbox" value="Select All" onclick="wbCdelSelectAll();" />
      <input type="button" style="display: none;" class="btn btn-xs btn-default" id="wbCdelDeselectAllCheckbox" name="wbCdelDeselectAllCheckbox" value="Deselect All" onclick="wbCdelDeselectAll();" />
      <br />
      <br />
      <?php  
       // We'll use this nonce field later on when saving.  
       wp_nonce_field( 'wb_stats_meta_box_nonce', 'meta_box_nonce' );       
         global $wpdb;
         $mediaList = $wpdb->get_results( 
            "
               SELECT *
               FROM wb_stats_dmedia
               WHERE status = 1
            " );
         
         $enabledNotifications = 0;
         foreach($mediaList as $currentMedia){
            $postIdToMedia = $wpdb->get_results( $wpdb->prepare( 
               "
                  SELECT *
                  FROM `wb_stats_post_dmedia` 
                  WHERE dmediaId = %d
                  AND postId = %d 
                  LIMIT 1
               ", 
               $currentMedia->dmediaId,
               $post->ID ) );    
                                 
      ?>
      <input type="checkbox" id="wb_stats_dmediaId<?php _e($currentMedia->dmediaId); ?>" name="wb_stats_dmediaId<?php _e($currentMedia->dmediaId); ?>" value="<?php _e($currentMedia->dmediaId); ?>" <?php 
                     if( count($postIdToMedia) > 0 ){ echo 'checked'; $enabledNotifications++; } ?>  onchange="cDelAddDMediaToPost(this);" />
      <label for="wb_stats_dmediaId<?php _e($currentMedia->dmediaId); ?>"><strong><?php _e($currentMedia->dmediaName); ?></strong></label> <br />
      <?php
      if( $currentMedia->postLimit > 0 ){
      ?>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
             $('#wb_stats_dmediapost<?php _e($currentMedia->dmediaId); ?>').bind("input paste", function(e) {
                 $(this).val($(this).val().substring(0,<?php _e($currentMedia->postLimit); ?>));
             });            
         });          
      </script>     
      <div style="<?php if(count($postIdToMedia) == 0) { echo 'display: none;'; } ?> margin-left: 70px; margin-top: 0" id="wb_stats_dmediapost<?php _e($currentMedia->dmediaId); ?>_div" >
         <p>Recommended Post <small>(Maximum <?php _e($currentMedia->postLimit); ?> characters)</small></p>
         <textarea id="wb_stats_dmediapost<?php _e($currentMedia->dmediaId); ?>" name="wb_stats_dmediapost<?php _e($currentMedia->dmediaId); ?>" style="width: 100%; height: 100px;"><?php echo $postIdToMedia[0]->recommendedPost; ?></textarea>   
      </div>
      
      <br />
      <?php   
      }      
      ?>
      
      <?php
         }
         if($enabledNotifications > 0){
      ?>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
             $('.wbCdelShowForm').show();   
             $('#wbCdelEnableCheckbox').attr('checked', 'checked');       
         });          
      </script>          
      <?php      
         }
      ?>
   </div>
   <?php
    $allLists = $wpdb->get_results( 
      "
         SELECT *
         FROM wb_stats_lists
         WHERE status = 1
         ORDER BY listName ASC
      " );   
    
    $selectedListResults = $wpdb->get_results( 
       $wpdb->prepare(
      "
         SELECT *
         FROM wb_stats_post_list
         WHERE postId = %d
      ", $post->ID ) );   
    
    $selectedListIds = array();
    foreach($selectedListResults as $currentList){
        $selectedListIds[] = $currentList->listId;
    }
    
   ?>
   <p class="wbCdelShowForm">Select the list you would like to send the notifications to.</p>
   <div class="wbCdelShowForm" style="margin-left: 30px; margin-bottom: 30px;">
        <select class="form-control" id="wb_stats_listIds" name="wb_stats_listIds[]" multiple="multiple">               
          <?php
          foreach($allLists as $currentList){
          ?>
          <option value="<?php echo $currentList->listId; ?>" <?php echo ( in_array($currentList->listId, $selectedListIds) ) ? ' selected="selected" ' : '' ; ?>><?php echo $currentList->listName; ?></option>
          <?php
          }
          ?>   
       </select>      
        <div style="text-align: center; width: 370px; margin: 10px 0;">
            <input  style="" class="btn btn-sm" type="button" value="Select All" onclick="$('#wb_stats_listIds').multiSelect('select_all');" />
            <input  style="" class="btn btn-sm" type="button" value="De-Select All" onclick="$('#wb_stats_listIds').multiSelect('deselect_all');" /> 
        </div>       
   </div>   
   <script type="text/javascript">
    $('#wb_stats_listIds').multiSelect();       
   </script>
<?php         
           * 
           */
      }
      
      
      function save_post_dmedia($post_id){
          /*
         global $wpdb; 
         
         // Bail if we're doing an auto save         
         if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
            return;          
         }  
         
         // if our nonce isn't there, or we can't verify it, bail
         if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'wb_stats_meta_box_nonce' ) ){
            return;
         }
         
         // if our current user can't edit this post, bail
         if( !current_user_can( 'edit_post' ) ) {
            return;
         }
         
         $selectedMedia = array();         
         
         foreach($_POST as $key => $value) {
             if (strpos($key, 'wb_stats_dmediaId') === 0) {
                 $tempArray = explode('wb_stats_dmediaId', $key);          
                 array_push($selectedMedia, absint($tempArray[1]) );
             }                             
         }  
         
         //remove all post to media entries
         $deletedRows = $wpdb->query( $wpdb->prepare( 
            "
               DELETE FROM `wb_stats_post_dmedia` 
               WHERE `wb_stats_post_dmedia`.`postId` = %d                
            ", 
            $post_id
         ) );             
         
                  
         if( count($selectedMedia) > 0 ){
            //insert selected dmedia into database   
            foreach($selectedMedia as $currentMediaId){
               $recommendedPost = $_POST['wb_stats_dmediapost'.$currentMediaId];
               
               $insertedRows = $wpdb->query( $wpdb->prepare( 
                  "
                     INSERT INTO `wb_stats_post_dmedia` (`postId`, `dmediaId`, `recommendedPost`) 
                     VALUES (%d, %d, %s)
                     ON DUPLICATE KEY UPDATE `recommendedPost` = %s
                  ", 
                  $post_id, 
                  $currentMediaId,
                  $recommendedPost,
                  $recommendedPost
               ) );            
            }               
         }
         
         $selectedLists = $_POST['wb_stats_listIds'];
         
         //remove all post to media entries
         $deletedRows = $wpdb->query( $wpdb->prepare( 
            "
               DELETE FROM `wb_stats_post_list` 
               WHERE `postId` = %d                
            ", 
            $post_id
         ) );             
         
                  
         if( count($selectedLists) > 0 ){
            //insert selected dmedia into database   
            foreach($selectedLists as $currentListId){                              
               $insertedRows = $wpdb->query( $wpdb->prepare( 
                  "
                     INSERT INTO `wb_stats_post_list` (`postId`, `listId`) VALUES (%d, %d);
                  ", 
                  $post_id, 
                  $currentListId
               ) );            
            }               
         }         
           * 
           */
         
      }
      
      
      function displayReport(){
        // require_once('pages/reports.php');
      }     
      
      function wb_stats_user_profile(){
          /*
         global $wpdb;
         $user_id = ( isset($_GET['user_id']) && is_numeric($_GET['user_id'])) ? $_GET['user_id'] : get_current_user_id() ;
         
         
         $user_meta = get_user_meta($user_id);
         $timezone = $user_meta['wb_stats_timezone'][0];         
      ?>
<link rel='stylesheet' id='wbStatsMultiSelectStyle-css'  href='/wp-content/plugins/workerbee-video-stats/js/multiselect/css/multi-select.css' type='text/css' media='all' />
<script type='text/javascript' src='/wp-content/plugins/workerbee-video-stats/js/multiselect/js/jquery.multi-select.js'></script>   
<h2><?php _e("Video Reports Settings", "blank"); ?></h2>

<table class="form-table">
   <tr style="display: none;">
      <th><label for="locationToShare"><?php _e("Location to Share"); ?></label></th>
      <td>
         <select style="width: 350px;" class="form-control" id="locationToShare" name="locationToShare">
            <option value="default"> -- </option>
            <option value="apf" selected="selected">APF</option>
            <option value="syndicatedVid">Syndicated Video</option>               
         </select>                 
      </td>
   </tr>
            
   <tr>
      <th><label for="timezone"><?php _e("Timezone"); ?></label></th>
      <td>
         <select class="form-control" style="width: 350px;" id="timezone" name="timezone" onchange="" required>
            <option value=""> -- </option>
            <?php
              $all = timezone_identifiers_list();    
                $i = 0;
                foreach($all AS $zone) {
                  $zone = explode('/',$zone);
                  $zonen[$i]['continent'] = isset($zone[0]) ? $zone[0] : '';
                  $zonen[$i]['city'] = isset($zone[1]) ? $zone[1] : '';
                  $zonen[$i]['subcity'] = isset($zone[2]) ? $zone[2] : '';
                  $i++;
                }
            
                asort($zonen);
                
                //rearrange the zones so that America comes first
                $americaArray = array();
                foreach($zonen as $key => $currentZonen){
                   if($currentZonen['continent'] == 'America' ){
                      $americaArray[] = $currentZonen;
                      unset($zonen[$key]);
                   }
                }
                
                $zonen = array_merge ($americaArray, $zonen);              
                                   
                $structure = '';
                foreach($zonen AS $zone) {
                  extract($zone);
                  if($continent == 'Africa' || $continent == 'America' || $continent == 'Antarctica' || $continent == 'Arctic' || $continent == 'Asia' || $continent == 'Atlantic' || $continent == 'Australia' || $continent == 'Europe' || $continent == 'Indian' || $continent == 'Pacific') {
                    if(!isset($selectcontinent)) {
                      $structure .= '<optgroup label="'.$continent.'">'; // continent
                    } elseif($selectcontinent != $continent) {
                      $structure .= '</optgroup><optgroup label="'.$continent.'">'; // continent
                    }
            
                    if(isset($city) != ''){
                      if (!empty($subcity) != ''){
                        $city = $city . '/'. $subcity;
                      }
                      $structure .= "<option ".((($continent.'/'.$city)==$timezone)?'selected="selected "':'')." value=\"".($continent.'/'.$city)."\">".str_replace('_',' ',$city)."</option>"; //Timezone
                    } else {
                      if (!empty($subcity) != ''){
                        $city = $city . '/'. $subcity;
                      }
                      $structure .= "<option ".(($continent==$timezone)?'selected="selected "':'')." value=\"".$continent."\">".$continent."</option>"; //Timezone
                    }
            
                    $selectcontinent = $continent;
                  }
                }
                $structure .= '</optgroup>';
              
              echo $structure;
            ?>    
         </select>
         <span class="description"><?php _e("Please select your timezone."); ?></span>
      </td>
   </tr>

   <?php
   //get selected distribution media
   $selectedMedia = array();
   $selectedMediaRows = $wpdb->get_results( $wpdb->prepare( 
      "
         SELECT * 
         FROM `wb_stats_amb_dmedia` 
         WHERE `ambassadorId` = %d
         AND `status` = 1
      ", 
      $user_id
   ) );       
   
   foreach($selectedMediaRows as $currentMedia){
      array_push($selectedMedia, absint($currentMedia->dmediaId) );
   }   
   
   $mediaList = $wpdb->get_results( 
      "
         SELECT *
         FROM wb_stats_dmedia
         WHERE status = 1
      " );
   
   if( count($mediaList) > 0 ){
   ?>
   <tr>
      <th><label for="city"><?php _e("Subscriptions"); ?></label></th>
      <td>      
         <p style="padding-top: 0px;" class="description indicator-hint"><?php _e("Select the Distribution Media you would like to subscribe to:"); ?></p>
         <br />
         <div class="form-group">
         <?php
         foreach($mediaList as $currentMedia){
         ?>
            <div class="col-sm-4">  
               <div class="">
                  <label for="dmediaId<?php _e($currentMedia->dmediaId); ?>">                  
                     <input type="checkbox" id="dmediaId<?php _e($currentMedia->dmediaId); ?>" name="dmediaId<?php _e($currentMedia->dmediaId); ?>" value="<?php _e($currentMedia->dmediaId); ?>" <?php 
                        if( in_array($currentMedia->dmediaId, $selectedMedia) ){ echo 'checked'; } ?>/>
                     <?php _e($currentMedia->dmediaName); ?>
                  </label>
               </div>
            </div>
            <br clear="all" /> 
            
         <?php   
         }
         ?>            
        </div>       
      </td>
   </tr>            
   <?php
   }
   ?>    
   <tr>
    <th>
         <label for="city"><?php _e("Lists <small>Select the list you would like to add the ambassador to.:</small>"); ?></label>       
    </th>
    <td>
      <?php
        $allLists = $wpdb->get_results( 
          "
             SELECT *
             FROM wb_stats_lists
             WHERE status = 1
             ORDER BY listName ASC
          " );   

        $selectedListResults = $wpdb->get_results( 
           $wpdb->prepare(
          "
             SELECT *
             FROM wb_stats_amb_list
             WHERE ambassadorId = %d
             AND status = 1
          ", $user_id ) );   

        $selectedListIds = array();
        foreach($selectedListResults as $currentList){
            $selectedListIds[] = $currentList->listId;
        }

       ?>
       <div class="form-group">
           
            <label for="locationToShare" class="col-sm-2 control-label"></label>
            <div class="col-sm-3">
            <select class="form-control" id="wb_stats_listIds" name="wb_stats_listIds[]" multiple="multiple">               
              <?php
              foreach($allLists as $currentList){
              ?>
              <option value="<?php echo $currentList->listId; ?>" <?php echo ( in_array($currentList->listId, $selectedListIds) ) ? ' selected="selected" ' : '' ; ?>><?php echo $currentList->listName; ?></option>
              <?php
              }
              ?>   
           </select>      
            <div style="text-align: center; width: 370px; margin: 10px 0;">
                <input  style="" class="btn btn-sm" type="button" value="Select All" onclick="jQuery('#wb_stats_listIds').multiSelect('select_all');" />
                <input  style="" class="btn btn-sm" type="button" value="De-Select All" onclick="jQuery('#wb_stats_listIds').multiSelect('deselect_all');" /> 
            </div>     
            </div>
       </div>   
       <script type="text/javascript">
        jQuery('#wb_stats_listIds').multiSelect();       
       </script>           
    </td>
        
    
   </tr>
</table>
      <?php    
           * 
           */        
      } //end of function
      
      function wb_stats_save_profile(){
          /*
         global $wpdb;
         $user_id = ( isset($_POST['user_id']) && is_numeric($_POST['user_id'])) ? $_POST['user_id'] : get_current_user_id() ;
         
         
         $selectedMedia = array();
         foreach($_POST as $key => $value) {
             if (strpos($key, 'dmediaId') === 0) {
                 $tempArray = explode('dmediaId', $key);          
                 array_push($selectedMedia, absint($tempArray[1]) );
             }    
         }          
         
   
         $timezone = sanitize_text_field($_POST['timezone']);
         $locationToShare = sanitize_text_field($_POST['locationToShare']);
         
            
         //update locationToShare
         update_user_meta( $user_id, 'wb_stats_locationToShare', $locationToShare);
         update_user_meta( $user_id, 'wb_stats_timezone', $timezone);

         
         //delete current dmedia
         $deleteRows = $wpdb->query( $wpdb->prepare( 
            "
            DELETE FROM `wb_stats_amb_dmedia` 
            WHERE `ambassadorId` = %d
            AND `status` = 1
            ", 
            $user_id
         ) );           
         
         
         //insert selected dmedia into database                   
         foreach($selectedMedia as $currentMediaId){
            $insertedRows = $wpdb->query( $wpdb->prepare( 
               "
                  INSERT INTO `wb_stats_amb_dmedia` 
                  ( `ambassadorId` , `dmediaId`, `dateAdded`, `dateDeleted`, `status` )
                  VALUES ( %d, %d, NOW(), '00-00-00 00:00:00',  1 );
               ", 
               $user_id, 
               $currentMediaId
            ) );            
         }   
         
         $selectedLists = $_POST['wb_stats_listIds'];
         
         //update status for removed
         $removedAmbassadorFromListCount = $wpdb->update( 
            'wb_stats_amb_list', 
            array(            
               'dateDeleted' => date('Y-m-j h:i:s'),
               'status' => 4
            ),
            array( 
               'ambassadorId' => $user_id,              
               'status' => 1
            ) 
         );               

         //add ambassador to lists
         foreach($selectedLists as $currentListId){
            $insertedRows = $wpdb->query( $wpdb->prepare( 
               "
                  INSERT INTO `wb_stats_amb_list` 
                  ( `ambassadorId` , `listId`, `dateAdded`, `dateDeleted`, `status` )
                  VALUES ( %d, %d, NOW(), '00-00-00 00:00:00',  1 );
               ", 
               $user_id, 
               $currentListId
            ) );                
         }         
               
           * 
           */
      }

function register_and_build_fields() {
    /*
    global $allPages, $wb_stats_options; 
	//register our settings
	register_setting( 'wb_stats_options', 'wb_stats_options', 'validate_setting' );
   add_settings_section('cdel_general_settings', 'General Settings', 'section_general', __FILE__);
   
   add_settings_section('notifications_settings', 'Notifications Settings', 'section_notification', __FILE__);
   
        $args = array(
           'sort_order' => 'ASC',
           'sort_column' => 'post_title',
           'hierarchical' => 1,
           'exclude' => '',
           'include' => '',
           'meta_key' => '',
           'meta_value' => '',
           'authors' => '',
           'child_of' => 0,
           'parent' => 0,
           'exclude_tree' => '',
           'number' => '',
           'offset' => 0,
           'post_type' => 'page',
           'post_status' => 'publish'
        );         
        $allPages = get_pages($args); 
        
        $pageCounter = 0;
        foreach($allPages as $currentPage){
            //echo '$currentPage is '.print_r($currentPage, true);
            $currentArgs = array(
                'sort_order' => 'ASC',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => $currentPage->ID,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish'
             );       
            //echo '$currentArgs is '.print_r($currentArgs, true);
            $allChildren = get_pages($currentArgs);
            
            //echo '$allChildren is '.print_r($allChildren, true);
            
            if( count($allChildren) > 0 ){
                $allPages[$pageCounter]->children = $allChildren;
            }            
               
            $pageCounter++;
            
        }   
   
    function section_notification() {
        echo 'Settings for the Email Notification to be sent to the Ambassadors.';
    }   
    
    function section_general() {?>
    General Settings for Video Reports Plugin
    <script type="text/javascript">        
        function updatePageInfo(selectElement){
            jQuery('#'+selectElement.id+'PageTitle').val( jQuery(selectElement).find(':selected').attr('data-title') );
            jQuery('#'+selectElement.id+'PageSlug').val( jQuery(selectElement).find(':selected').attr('data-slug') );
        }        
        jQuery(document).ready( function($) {
            jQuery('#theme-options-wrap input[type="checkbox"]').change(function() {
               if( this.checked ){
                  this.value = true;
               }
               else{
                  this.value = false;
               }
            });
        });
    </script>
    <?php    
    }       
   
   
   $wb_stats_options = get_option('wb_stats_options');
     * 
     */
   
}


function wbStatsSettingsPage() {
    /*
?>
<div id="theme-options-wrap">
    <div class="icon32" id="icon-tools"> <br /> </div>
    <h2>Video Reports Settings</h2>
    <p>Update settings for the Video Reports plugin.</p>
    <form method="post" action="options.php" enctype="multipart/form-data">
        <?php settings_fields('wb_stats_options'); ?>
        <?php do_settings_sections(__FILE__); ?>
        <p class="submit">
            <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
        </p>
    </form>
</div>
<?php
     * 
     */
}      
      

   }
   
}//End Class Workerbee_Video_Stats

if (class_exists("Workerbee_Video_Stats")) {
   global $wb_stats;
   $wb_stats = new Workerbee_Video_Stats();
}


//Initialize the admin panel
if (!function_exists("Workerbee_Video_Stats_ap")) {
   function Workerbee_Video_Stats_ap() {
      global $wb_stats;
      if (!isset($wb_stats)) {
         return;
      }
      
      $dashboardpage = add_menu_page('Video Reports', 'Video Reports', 'upload_files', basename(__FILE__), array(&$wb_stats, 'displayDashboard'), 'dashicons-chart-line');
      $ga_page = add_submenu_page( 'workerbee-video-stats.php', 'Google Analytics', 'Google Analytics', 'upload_files', 'wb-vs-google-analytics', array(&$wb_stats, 'displayGA') );      
                  
      add_action('admin_print_scripts-' . $ga_page, array(&$wb_stats, 'wb_scripts'));
      
      
   }     
}




//Actions and Filters
if (isset($wb_stats)) {
   //Actions
   //add_action( 'wp_enqueue_scripts', array(&$wb_stats, 'load_scripts') );
   
   add_action( 'activate_workerbee-video-stats/workerbee-video-stats.php',  array(&$wb_stats, 'init'));
   add_action( 'admin_menu', 'Workerbee_Video_Stats_ap');
    /*
   add_action( 'add_meta_boxes', array(&$wb_stats, 'wb_stats_add_meta_box') );
   add_action( 'save_post', array(&$wb_stats, 'save_post_dmedia') );
   
   add_action( 'show_user_profile', array(&$wb_stats, 'wb_stats_user_profile') );
   add_action( 'edit_user_profile', array(&$wb_stats, 'wb_stats_user_profile') );   
   
   add_action( 'personal_options_update', array(&$wb_stats, 'wb_stats_save_profile') );
   add_action( 'edit_user_profile_update', array(&$wb_stats, 'wb_stats_save_profile') );   
   
   add_action( 'admin_init',  array(&$wb_stats,'register_and_build_fields') );
   add_action('admin_head', 'wb_stats_remove_dashmenu' );
     * 
     */
   
}

    
        
       
    
function wb_stats_remove_dashmenu(){     
    $current_user = wp_get_current_user();
    $current_user_roles = $current_user->roles;
    
    echo '<!-- $current_user_roles is '.print_r($current_user_roles, true).' -->';

    if ( is_user_logged_in() && !in_array( 'administrator', $current_user_roles) && !in_array( 'editor', $current_user_roles) && !in_array( 'author', $current_user_roles) && !in_array( 'contributor', $current_user_roles) && !in_array( 'shop_manager', $current_user_roles) ) {
        echo '<style type="text/css">#adminmenu li { display: none; } #menu-users { display: inherit !important; } #update-nag, .update-nag {display: none !important; }</style>';  
    }
}    

if( !function_exists('sendEmail') ){
    function sendEmail($subject, $messageHeading, $messageBody, $recipient, $sender, $replyTo = ''){
       global $useInternal, $debugMode, $wb_ent_options;

       $headers = "From: $sender\n";

       if( trim($replyTo) != '' ){
          $headers .= "Reply-To: $replyTo\n";
       }

       $headers .= 'MIME-Version: 1.0' . "\r\n";
       $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n"; 
       $headers .= 'Bcc: jullie.quijano@workerbee.tv' . "\r\n";

       $message = array();

       $message[] ="<!DOCTYPE html>";
       $message[] ="<html lang=\"en\">";
       $message[] ="<head>";
       $message[] ="<title>{$wb_ent_options['channelname']}</title>";
       $message[] ="<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
       $message[] ="<meta name=\"description\" content=\"\">";
       $message[] ="<meta name=\"author\" content=\"\">";
       $message[] ="</head>";
       $message[] ="<body style='margin: 10px auto; width: 660px;'>";
       $message[] ="<div style='background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif; font-size: 14px; line-height: 20px; margin: 0px; padding-top: 0px;'>";
       $message[] ="  <div>";
       $message[] ="     <div style=\"width: 250px; text-align: center;\">";
       $message[] ="        <img src=\"http://".$wb_ent_options['channeldomain'].'/'.$wb_ent_options['loginlogo']."?v=".time()."\" alt=\"{$wb_ent_options['channelname']}\" style=\"width: 250px\">";
       $message[] ="     </div>";
       $message[] ="  </div>";
       $message[] ="  <br clear=\"all\" />";
       $message[] ="  <div>";
       $message[] ="      <h1>$messageHeading</h1>";
       $message[] ="      <div>";
       $message[] ="         $messageBody";
       $message[] ="      </div>";
       //$message[] ="     <p style='margin-top: 30px; margin-bottom: 5px;'><small>You are receiving this confirmation email because you have registered as an {$wb_ent_options['channelname']} user. As a registered user, you will receive email communications for purchased video products, and periodic alerts that may included promotional and educational content.</small></p>";
       $message[] ="   </div>";
       $message[] ="</div>";
       $message[] ="</body>";
       $message[] ="</html>";

       $message =  implode("\n",$message);

       if( !mail($recipient,$subject,$message,$headers) ) {                
          mail('jullie.quijano@workerbee.tv','Registration confirmation email not sent', '', $headers);
       }

    }
}

function sec2hms ($sec, $padHours = false){
	// start with a blank string
	$hms = "";
	
	// do the hours first: there are 3600 seconds in an hour, so if we divide
	// the total number of seconds by 3600 and throw away the remainder, we're
	// left with the number of hours in those seconds
	$hours = intval(intval($sec) / 3600); 
	
	// add hours to $hms (with a leading 0 if asked for)
	$hms .= ($padHours) 
	      ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"
	      : $hours. ":";
	
	// dividing the total seconds by 60 will give us the number of minutes
	// in total, but we're interested in *minutes past the hour* and to get
	// this, we have to divide by 60 again and then use the remainder
	$minutes = intval(($sec / 60) % 60); 
	
	// add minutes to $hms (with a leading 0 if needed)
	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";
	
	// seconds past the minute are found by dividing the total number of seconds
	// by 60 and using the remainder
	$seconds = intval(round($sec) % 60); 
	//$seconds = intval(ceil($sec) % 60); 
	
	
	// add seconds to $hms (with a leading 0 if needed)
	$hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
	
	// done!
	return $hms;
}