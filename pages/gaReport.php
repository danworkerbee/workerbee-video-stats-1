<?php
global $wpdb;
$wb_stats_options = get_option('wb_stats_options');
$wb_ent_options = get_option('wb_ent_options');
//authenticate with google
// Creates and returns the Analytics service object.

// Load the Google API PHP Client Library.

require_once wbStatsPATH.'/google-php-oauth2/google-api-php-client/src/Google/autoload.php';

// Use the developers console and replace the values with your
// service account email, and relative location of your key file.
$service_account_email = '937696055323-1en00pn3tesfb7hhekcgvbkvdl3c253f@developer.gserviceaccount.com';
$key_file_location = wbStatsPATH.'/google-php-oauth2/Video Reporting Plugin-547a98ae4151.p12';

try{
	$config = new Google_Config();
	$config->setClassConfig('Google_Cache_File', array('directory' => '../tmp/cache'));
	// Here I set a relative folder to avoid pb on permissions to a folder like /tmp that is not permitted on my mutualised host
	
	$client = new Google_Client($config);
	$client->setApplicationName("WBAnalytics");
	
	// Read the generated client_secrets.p12 key.
	$key = file_get_contents($key_file_location);
	$cred = new Google_Auth_AssertionCredentials(
			$service_account_email,
			array(Google_Service_Analytics::ANALYTICS_READONLY),
			$key
			);
	$client->setAssertionCredentials($cred);
	if($client->getAuth()->isAccessTokenExpired()) {
		$client->getAuth()->refreshTokenWithAssertion($cred);
	}
	
	$analytics = new Google_Service_Analytics($client);
	//echo '<!-- $analytics is '.print_r($analytics, true).' -->';
} catch (Exception $ex) {
	//echo '<!-- $ex is '.print_r($ex, true).' -->';
}


if( isset($_GET['display']) ){
	$page = $_GET['display'] ;
}else{
	$page = "mostRecentVideos";
}

$hasError = false;
$errorMessage = '';

$minimumDate = date('Y-m-d', strtotime('January 1, 2005'));

$rangeFrom 	= date('Y') . '-01-01';
$rangeTo 	= date('Y-m-d');

if( isset($_GET['rangeFrom']) ){
	$rangeFrom = $_GET['rangeFrom'] ;
}

if( isset($_GET['rangeTo']) ){
	$rangeTo = $_GET['rangeTo'] ;
}


if( isset($_GET['s']) && trim($_GET['s']) != '' ){
	$searchWord = $_GET['s'] ;
}
elseif( isset($_GET['s']) && trim($_GET['s']) == ''){
	$searchWord = "";
	if( $page == 'search' ){
		$errorMessage = 'Please enter a search word.';
		$hasError = true;
	}
}

if( $rangeFrom < $minimumDate || $rangeTo < $minimumDate ){
	$errorMessage = 'Please select a date after January 1, 2005.';
	$hasError = true;
}


if( isset($_GET['vidCat']) ){
	$vidCat = $_GET['vidCat'] ;
}else{
	$vidCat = "";
}

$csvFileName = str_replace(" ", "_", $wb_ent_options['channelname'].' Google Analytics Report '.$rangeFrom.' to '.$rangeTo.'.csv');
$videoListToCsv = array();
$videoListToCsv[] = array('Date Published', 'Post Title', 'Link', 'Page Views', 'Unique Page Views', 'Average Time On Page');


//include_once('header.php');
include_once(wbStatsPATH . '/resources/config.php');
include_once(wbStatsPATH . '/resources/functions.php');
include_once(wbStatsPATH. '/pages/header.php');

global $wpdb;


//prevent csrf attacks
$nonce= wp_create_nonce('my-nonce');

$shortcodeChannel = $wb_ent_options['vidshortcode'];

$passwordProtectedCondition = " AND p.post_password   = '' ";
if( $wb_stats_options['displayPasswordProtectedVideos'] === true ){
	$passwordProtectedCondition = '';
}
$privateAndUnlistedPostIds = array();
if( $wb_stats_options['displayPrivateVideos'] === true || $wb_stats_options['displayUnlistedVideos'] === true ){
	$catUnlisted = isset($wb_ent_options['videocats']['unlisted']) ? $wb_ent_options['videocats']['unlisted'] : 0 ;
	$catPrivate = isset($wb_ent_options['videocats']['private']) ? $wb_ent_options['videocats']['private'] : 0 ;
	
	//get private and unlisted videos
	if( $wb_ent_options['vidshortcode']['enabled'] ){
		$getPrivateAndUnlistedVideos = $wpdb->get_results( $wpdb->prepare("
				SELECT p.ID
				FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
				WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
				AND tt.term_id=t.term_id
				AND tr.object_id=p.ID
				AND p.post_status='publish'
				$passwordProtectedCondition
				AND ( t.term_id=" . $catUnlisted . " OR t.term_id=" . $catPrivate . ")
             GROUP BY p.ID
             ORDER BY p.post_date DESC
           "), ARRAY_A);
	}
	else{
		$getPrivateAndUnlistedVideos = $wpdb->get_results( $wpdb->prepare("
				SELECT p.ID
				FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
				WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
				AND tt.term_id=t.term_id
				AND tr.object_id=p.ID
				AND p.ID=b.post_id
				$passwordProtectedCondition
				AND p.post_status='publish'
				AND ( t.term_id=" . $catUnlisted . " OR t.term_id=" . $catPrivate . ")
             GROUP BY p.ID
             ORDER BY p.post_date DESC
           "), ARRAY_A);
	}
	
	foreach($getPrivateAndUnlistedVideos as $currentPost){
		$privateAndUnlistedPostIds[] = $currentPost['ID'];
	}
}

if ($privateAndUnlistedPostIds){
	$wb_exclude_posts = " AND p.ID NOT IN (" . implode (", ", $privateAndUnlistedPostIds) . ") ";
}else{
	$wb_exclude_posts = "";
}

$searchTabClass = '';
$mostRecentTabClass = '';
$allVideosTabClass = '';
$vidCatTabClass ='';
$vidCatKeyTabClass ='';

switch($page){
	case 'videoCat':
		$vidCatTabClass = 'class="active"';
		break;
	case 'search':
		$searchTabClass = 'class="active"';
		break;
	case 'videoCatKey':
		$vidCatKeyTabClass = 'class="active"';
		break;
	case 'mostRecentVideos':
		$mostRecentTabClass = 'class="active"';
		break;
	case 'allVideos':
		$allVideosTabClass = 'class="active"';
		break;
        case 'videoTag':
                $vidTagTabClass =  'class="active"';
		break;
}
?>
<style type="text/css">
  #report_details table{
    margin: auto;
  }
  #report_details table th,#report_details table td{
    text-align: center;
    border: 1px solid #aaa;
    padding: 10px;
    text-align: center;
  }
  div.row {
    margin-right: 0px;
  }
  .wb-alert-box{
  	max-width: 360px;
  }
</style>
<h2>Google Analytics Report</h2>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-12">  
            <ul class="nav nav-tabs">
              <!--<li <?php echo $mostRecentTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=mostRecentVideos&rangeFrom=<?php echo $rangeFrom; ?>&rangeTo=<?php echo $rangeTo; ?>">Most Recent Videos</a></li>-->
              <li <?php echo $mostRecentTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=mostRecentVideos">Most Recent Videos</a></li>
              <li <?php echo $searchTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=search&rangeFrom=<?php echo $rangeFrom; ?>&rangeTo=<?php echo $rangeTo; ?>">Search</a></li>
              <li <?php echo $vidCatTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=videoCat&rangeFrom=<?php echo $rangeFrom; ?>&rangeTo=<?php echo $rangeTo; ?>">Search By Category</a></li> 

              <li <?php echo $vidCatKeyTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=videoCatKey&rangeFrom=<?php echo $rangeFrom; ?>&rangeTo=<?php echo $rangeTo; ?>">Search By Category Keyword</a></li>                                          
              <?php  
              /*
              <li <?php echo $allVideosTabClass; ?>><a href="/wp-admin/admin.php?page=wb-vs-google-analytics&display=allVideos&rangeFrom=<?php echo $rangeFrom; ?>&rangeTo=<?php echo $rangeTo; ?>">All Videos</a></li>  
               * 
               */?>
            </ul>
        </div>    
    </div>    
</div>    
<?php
if($page == 'search' ){
	$wb_no_error 		= true;
	$wb_date_error	 	= ' style="display: none;"';
	$wb_string_error 	= ' style="display: none;"';
	
	if ( $rangeFrom > $rangeTo ){
		$wb_no_error 	= false;
		$wb_date_error 	= '';
	}
	
	if ( trim($searchWord) == "" && isset($_GET['s']) ){
		$wb_no_error 		= false;
		$wb_string_error 	= '';
	}
?>   
<script type="text/javascript">
$(document).ready(function(){
    $('#s').keypress(function(e) {
        if (e.which == 13) {
			$("#stat-loading-image").show();
			$(".wb-stat-buttons").attr("disabled", true);
            location.href = '/wp-admin/admin.php?page=wb-vs-google-analytics&display=<?php echo $page; ?>&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&s='+$('#s').val()
        }
    });  
    
    $( "#report-submit-button-search" ).click(function() {
    	var wb_search_link='/wp-admin/admin.php?page=wb-vs-google-analytics&display=<?php echo $page; ?>&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&s='+$('#s').val();
			$("#stat-loading-image").show();
			$(".wb-stat-buttons").attr("disabled", true);
			location.href = wb_search_link;
    });
  
});
</script>    
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-12">  
            <form> 
            	<input type="hidden" name="page"  value="wb-vs-google-analytics" />
                <input type="hidden" name="display" value="<?php echo $page; ?>" />                      
                <h4>Select the date range for your report</h4>
                <div class="alert alert-danger wb-alert-box" <?php echo $wb_date_error; ?>>
					<strong>Invalid date!</strong> Please check From and To dates.
				</div>
                From: <input type="date" id="rangeFrom" name="rangeFrom"
				       value="<?php echo $rangeFrom; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
                To: <input type="date" id="rangeTo" name="rangeTo"
				       value="<?php echo $rangeTo; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
				<h4>Enter a search word</h4>          
				<div class="alert alert-danger wb-alert-box" <?php echo $wb_string_error; ?>>
					<strong>No search string!</strong> Please fill search word.
				</div>       
                <input type='text' name="s" id="s" value="<?php echo $searchWord; ?>" />
                <!--  <a class="btn btn-primary" href="javascript: location.href = '/wp-admin/admin.php?page=wb-vs-google-analytics&display=<?php echo $page; ?>&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&s='+$('#s').val()">Search Videos</a>           
                 -->
                <a class="btn btn-primary wb-stat-buttons"  id="report-submit-button-search">Search Videos</a><img src="<?php echo plugins_url() . "/workerbee-video-stats/images/circle-loading-gif.gif" ;?>" id="stat-loading-image" style="display: none;width: 34px;margin-left: 10px;" />           
            </form>            
        </div>
    </div>
</div>        
    <br />
<?php
	if( trim($searchWord) != '' && $wb_no_error ){
            $allPosts = $wpdb->get_results( $wpdb->prepare("
                SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, p.post_date 
                   FROM wp_posts p
                   WHERE post_type = 'post' 
                   AND (p.post_status='publish')  
				   AND DATE(p.post_date) <= '".$rangeTo."' 
                   AND (p.post_title LIKE '%s'
                   OR p.post_content LIKE '%s')
                   $passwordProtectedCondition
                   ORDER BY p.post_date DESC                       
               ", '%'.$searchWord.'%', '%'.$searchWord.'%'), ARRAY_A); 
         
        $postsBySlug = array();
        $filterArray = array();
        
        $tempFilterStringArray = array();
        $tempcounter = 0;

        foreach($allPosts as $currentPost){           
            $postsBySlug[$currentPost['post_name']] = $currentPost;
            $filterArray[] = 'ga:pagePath=~/'.$currentPost['post_name'];
            //echo '<!-- $filterArray is '.print_r($filterArray, true).' -->';
        
            if( ($tempcounter != 0 && $tempcounter % 10 == 0) || ($tempcounter == ( count($allPosts) - 1)) ){
               $filterString = implode(',', $filterArray);    
               //echo '<!-- $filterString is '.$filterString.' -->';
               $tempFilterStringArray[$tempcounter] = str_replace( array('%20','+'), ' ', $filterString);
               $filterArray = array();
            }
            $tempcounter++;
        }

        $dimensions = array('pageTitle','pagePath','hostname');
        $metrics    = array('pageviews', 'uniquePageviews', 'avgTimeOnPage');       

        foreach( $tempFilterStringArray as $currentfilterStringArray ){

						try{
							$optParams = array(
									'dimensions' => 'ga:pageTitle,ga:pagePath,ga:hostname',
									'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',      
									'filters' => $currentfilterStringArray,
									'max-results' => '1000');

							$gaResults = $analytics->data_ga->get(
									'ga:'.wbStatsGAViewId,
									$rangeFrom,
									$rangeTo,
									'ga:sessions',
									$optParams);

						} catch (Exception $ex) {
							echo '<!-- $ex is '.print_r($ex, true).' -->';
						}	
						
						//echo '<!-- $gaResults is '.print_r($gaResults, true).' -->';
   	
   						foreach($allPosts as $currentPost){
							$wb_current_post_name = '/'.$currentPost['post_name'];
							$wb_current_post_name_length = strlen($wb_current_post_name);
				           	
				           	foreach($gaResults->rows as $result){
				           		$slugNoSlash = str_replace(array('/update_ga/','/'), '', $result[1]);
				           		$wb_result_postname = substr($slugNoSlash,0,$wb_current_post_name_length);	
                                                        echo "\r\n".'<!- $wb_current_post_name is -'.$wb_current_post_name.'- $wb_result_postname is -'.$wb_result_postname.'- -->';
				           		if ( $wb_current_post_name == $wb_result_postname){
					               	$postsBySlug[$wb_current_post_name]['pageViews'] = $postsBySlug[$wb_current_post_name]['pageViews'] + $result[3];
					               	$postsBySlug[$wb_current_post_name]['uniquePageViews'] = $postsBySlug[$wb_current_post_name]['uniquePageViews'] + $result[4];
					               	$postsBySlug[$wb_current_post_name]['aveTimeOnPage'] = $postsBySlug[$wb_current_post_name]['aveTimeOnPage'] + ( $result[5] * $result[3] );
					               	$postsBySlug[$wb_current_post_name]['wbctr']++;
				           		}
					    	}	
					    	
        				}
        
        
        
        }
        ?>
    <br />    
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12">                
                <table class="table table-striped table-bordered table-hover" id="gaResults">
                    <thead>
                    <tr>
                        <th class="text-center">Date Published</th>
                        <th class="text-center">Post Title</th>
                        <th class="text-center">Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The total number of pages viewed."></span></th>
                        <th class="text-center">Unique Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The pageviews that are generated by the same user during the same session."></span></th>
                        <th class="text-center">Average Time On Page</th>
                    </tr>
                    </thead>                
        <?php                
        if( count($allPosts) > 0 && count($gaResults->rows) > 0 ){            
            echo '<tbody>';
            foreach($postsBySlug as $currentPost){
            	
            	if ( defined( 'wb_how_to_videos_category' )){
            		if ( in_category ( wb_how_to_videos_category, $currentPost['ID'])){
            			continue;
            		}
            	}

                if( (!$wb_stats_options['displayPrivateVideos'] || !$wb_stats_options['displayUnlistedVideos']) && in_array($currentPost['ID'], $privateAndUnlistedPostIds)){
                    continue;
                }           
                
                echo '<tr>';
                echo '<td class="text-center">'.date("F j, Y", strtotime($currentPost['post_date'])).'</td>';
                echo '<td><a href="/'. $currentPost['post_name'].'" target="_BLANK">'.$currentPost['post_title'].'</a></td>';
                echo '<td class="text-center">'.( ( trim($currentPost['pageViews']) != '' ) ? ($currentPost['pageViews']) : '-' ).'</td>';
                echo '<td class="text-center">'.( ( trim($currentPost['uniquePageViews']) != '' ) ? ($currentPost['uniquePageViews']) : '-' ).'</td>';
                echo '<td class="text-center">'.( ( trim($currentPost['aveTimeOnPage']) != '' ) ? (sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews']))) : '-' ).'</td>';
                
                echo '</tr>';                         
								$videoListToCsv[] = array(date("F j, Y", strtotime($currentPost['post_date'])), 
									 $currentPost['post_title'], 
									 "http://".$wb_ent_options[channeldomain]."/".$currentPost['post_name'], 
									 $currentPost['pageViews'], 
									 $currentPost['uniquePageViews'], 
									 sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews'])));
               
            }      
            echo '</tbody>';
        }
        else{
        ?>
                    <tbody>
                        <tr><td style="border-right: 0px none;">There are no videos to display.</td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none;"></td>
                        </tr>
                    </tbody>
        <?php
        }
        ?>
                </table>
            </div>
        </div>
    </div>
    <?php
    
    }
        
}
elseif( $page == 'videoCat'  ){
	
	//if ( $_SERVER['REMOTE_ADDR'] == '64.141.26.78' ){
	//	echo "wb " ;
	//	exit();
	//}
    
	$wb_no_error 		= true;
	$wb_date_error	 	= ' style="display: none;"';
	$wb_category_error 	= ' style="display: none;"';
	
	if ( $rangeFrom > $rangeTo ){
		$wb_no_error 	= false;
		$wb_date_error 	= '';
	}
	
	if ( trim( $vidCat ) == "-1" && isset($_GET['vidCat']) ){
		$wb_no_error 		= false;
		$wb_category_error	= '';
	}
	
	if( $vidCat != '' && $vidCat !='all' ){
		$selectedCat = $vidCat;
	}else{
		$selectedCat = false;
	}
    
    $args = array(                
        'show_option_none' => 'Select a category',
        'orderby'            => 'NAME', 
        'order'              => 'ASC',                
        'child_of'           => 0,                
        'hierarchical'       => 1, 
        'name'               => 'vidCat',
        'id'                 => 'vidCat',
        'class'              => 'categoryList',
        'depth'              => 3,
        'taxonomy'           => 'category',
        'hide_if_empty'      => false,
        'selected'           => $selectedCat,
   		'exclude'			 => wb_how_to_videos_category
     );
        
?>    
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-12">  
            <form>   
            	<input type="hidden" name="page"  value="wb-vs-video-provider" />
                <input type="hidden" name="display" value="<?php echo $page; ?>" />                 
                <h4>Select the date range for your report</h4>
                <div class="alert alert-danger wb-alert-box" <?php echo $wb_date_error; ?>>
					<strong>Invalid date!</strong> Please check From and To dates.
				</div>
               	From: <input type="date" id="rangeFrom" name="rangeFrom"
				       value="<?php echo $rangeFrom; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
                To: <input type="date" id="rangeTo" name="rangeTo"
				       value="<?php echo $rangeTo; ?>"
				       min="2001-01-01" max="<?php echo date("Y-m-d"); ?>">
                <h4>Select a category from the list</h4>
                <div class="alert alert-danger wb-alert-box" <?php echo $wb_category_error; ?>>
					<strong>Invalid category!</strong> Please select a category.
				</div>
                <?php
                wp_dropdown_categories( $args );            
                ?>    
                <script type="text/javascript">
                	<?php 
                	if ( $vidCat == 'all' ){
                	?>
                		$("<option value='all' selected='selected'>All Videos</option>").insertAfter($("#vidCat option:first"));
                	<?php 	
                	}else{
                	?>
                		$("<option value='all'>All Videos</option>").insertAfter($("#vidCat option:first"));
                	<?php 
               	 		}
                	?>

                	$(document).ready(function(){
	                	$( "#report-submit-button" ).click(function() {
	                		var wb_link='/wp-admin/admin.php?page=wb-vs-google-analytics&display=videoCat&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&vidCat='+$('#vidCat').val();
	             			$("#stat-loading-image").show();
	             			$(".wb-stat-buttons").attr("disabled", true);
	             			location.href = wb_link;
	                    });
                	});
                    
                </script>
                <!-- <a class="btn btn-primary" onclick="cat_link_report();" href="javascript: location.href = '/wp-admin/admin.php?page=wb-vs-google-analytics&display=<?php echo $page; ?>&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&vidCat='+$('#vidCat').val()">Search Videos</a>
                 -->
                <a class="btn btn-primary wb-stat-buttons" id="report-submit-button">Search Videos</a><img src="<?php echo plugins_url() . "/workerbee-video-stats/images/circle-loading-gif.gif" ;?>" id="stat-loading-image" style="display: none;width: 34px;margin-left: 10px;" />
            </form>            
        </div>
    </div>
</div>        
    <br />
<?php
	if( trim($vidCat) != '' && $wb_no_error){
        //get all the published posts
            if( $vidCat == "all" ){
                    $allPosts = $wpdb->get_results( $wpdb->prepare("
                    SELECT  p.ID, p.post_title, p.post_name, p.post_content, p.post_password, p.post_date
                    FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                    WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                    AND tt.term_id=t.term_id
                    AND tr.object_id=p.ID
                    AND p.post_status='publish'
                                    AND DATE(p.post_date) <= '".$rangeTo."'
                            $passwordProtectedCondition
                            AND p.post_type='post'
                            GROUP BY p.ID
                            ORDER BY p.post_date DESC"), ARRAY_A);       
            }else{
                $allPosts = $wpdb->get_results( $wpdb->prepare("
                    SELECT  p.ID, p.post_title, p.post_name, p.post_content, p.post_password, p.post_date 
                    FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                    WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                    AND tt.term_id=t.term_id
                    AND tr.object_id=p.ID
                    AND p.post_status='publish'
                                    AND DATE(p.post_date) <= '".$rangeTo."'
                    $passwordProtectedCondition
                    AND p.post_type='post'
                    AND t.term_id='%s'
                    GROUP BY p.ID
                    ORDER BY p.post_date DESC                    
                   ", $vidCat), ARRAY_A);         
            }

        $postsBySlug = array();
        $filterArray = array();
        
        $tempFilterStringArray = array();
        $tempcounter = 0;
        foreach($allPosts as $currentPost){           
            $postsBySlug[$currentPost['post_name']] = $currentPost;
            $filterArray[] = 'ga:pagePath=~/'.$currentPost['post_name'];
            //$filterArray[] = 'ga:pagePath==/'.$currentPost['post_name']."?.*";
        
            if( ($tempcounter != 0 && $tempcounter % 10 == 0) || ($tempcounter == ( count($allPosts) - 1)) ){
               $filterString = implode(',', $filterArray);    
               $tempFilterStringArray[$tempcounter] = str_replace( array('%20','+'), ' ', $filterString);
               $filterArray = array();
            }
            $tempcounter++;
        }

        $dimensions = array('pageTitle','pagePath','hostname');
        $metrics    = array('pageviews', 'uniquePageviews', 'avgTimeOnPage');       
        foreach( $tempFilterStringArray as $currentfilterStringArray ){
						try{
							$optParams = array(
									'dimensions' => 'ga:pageTitle,ga:pagePath,ga:hostname',
									'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',      
									'filters' => $currentfilterStringArray,
									'max-results' => '1000');

							$gaResults = $analytics->data_ga->get(
									'ga:'.wbStatsGAViewId,
									$rangeFrom,
									$rangeTo,
									'ga:sessions',
									$optParams);
							//echo '<!-- $gaResults is '.print_r($gaResults, true).' -->';
						} catch (Exception $ex) {
							//echo '<!-- $ex is '.print_r($ex, true).' -->';
						}		
				
		   foreach($allPosts as $currentPost){     
		   		$wb_current_post_name = $currentPost['post_name'];
		   		$wb_current_post_name_length = strlen($wb_current_post_name);
		   		
		   		foreach($gaResults->rows as $result){
	               $slugNoSlash = str_replace(array('/update_ga/','/'), '', $result[1]);
	               $wb_result_postname = substr($slugNoSlash,0,$wb_current_post_name_length);	
	               echo "\r\n".'<!-- $wb_current_post_name is '.$wb_current_post_name.' $wb_result_postname is '.$wb_result_postname.' -->';
	               if ( $wb_current_post_name == $wb_result_postname){
	               	$postsBySlug[$wb_current_post_name]['pageViews'] = $postsBySlug[$wb_current_post_name]['pageViews'] + $result[3];
	               	$postsBySlug[$wb_current_post_name]['uniquePageViews'] = $postsBySlug[$wb_current_post_name]['uniquePageViews'] + $result[4];
	               	$postsBySlug[$wb_current_post_name]['aveTimeOnPage'] = $postsBySlug[$wb_current_post_name]['aveTimeOnPage'] + ( $result[5] *  $result[3] );
	               	$postsBySlug[$wb_current_post_name]['wbctr']++;
	               }

	       		}   
       	    
		   
		   }
            
        }
          
        ?>
    <br />    
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12">                
                <table class="table table-striped table-bordered table-hover" id="gaResults">
                    <thead>
                    <tr>
                        <th class="text-center">Date Published</th>
                        <th class="text-center">Post Title</th>
                        <th class="text-center">Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The total number of pages viewed."></span></th>
                        <th class="text-center">Unique Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The pageviews that are generated by the same user during the same session."></span></th>
                        <th class="text-center">Average Time On Page</th>
                    </tr>
                    </thead>                
        <?php                
        if( count($allPosts) > 0 ){            
            echo '<tbody>';
            foreach($postsBySlug as $currentPost){
            	
            	if ( defined( 'wb_how_to_videos_category' )){
            		if ( in_category ( wb_how_to_videos_category, $currentPost['ID'])){
            			continue;
            		}
            	}

                if( (!$wb_stats_options['displayPrivateVideos'] || !$wb_stats_options['displayUnlistedVideos']) && in_array($currentPost['ID'], $privateAndUnlistedPostIds)){
                    continue;
                }           
                
                echo '<tr>';
                echo '<td class="text-center">'.date("F j, Y", strtotime($currentPost['post_date'])).'</td>';
                echo '<td><a href="/'. $currentPost['post_name'].'" target="_BLANK">'.$currentPost['post_title'].'</a></td>';
                echo '<td class="text-center">'.( ( trim($currentPost['pageViews']) != '' ) ? ($currentPost['pageViews']) : '-' ).'</td>';
                echo '<td class="text-center">'.( ( trim($currentPost['uniquePageViews']) != '' ) ? ($currentPost['uniquePageViews']) : '-' ).'</td>';
                echo '<td class="text-center">'.( ( trim($currentPost['aveTimeOnPage']) != '' ) ? (sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews']))) : '-' ).'</td>';
                echo '</tr>';                         

            $videoListToCsv[] = array(date("F j, Y", strtotime($currentPost['post_date'])), 
               $currentPost['post_title'], 
               "https://".$wb_ent_options[channeldomain]."/".$currentPost['post_name'], 
               $currentPost['pageViews'], 
               $currentPost['uniquePageViews'], 
               (sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews']))));
               
            }      
            echo '</tbody>';
        }
        else{
        ?>
                    <tbody>
                        <tr>
                            <td style="border-right: 0px none;">There are no videos to display.</td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none;"></td>
                        </tr>
                    </tbody>
        <?php
        }
        ?>
                </table>
            </div>
        </div>
    </div>
    <?php
   
    }

}
elseif($page == 'mostRecentVideos' ){
   
	$rangeFrom 	= '2005-01-01';
 	$rangeTo 	= date('Y-m-d', strtotime('now'));
  
        $allPosts = $wpdb->get_results("
            SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, p.post_date 
               FROM wp_posts p
               WHERE post_type = 'post' 
				$wb_exclude_posts
               AND (p.post_status='publish') 
               $passwordProtectedCondition
               ORDER BY p.post_date DESC
               LIMIT 10
           ", ARRAY_A);                     
    
    $postsBySlug = array();
    $filterArray = array();
    foreach($allPosts as $currentPost){
        $postsBySlug[$currentPost['post_name']] = $currentPost;
        $filterArray[] = 'ga:pagePath==/'.$currentPost['post_name'];
    }
    
    $filterString = implode(',', $filterArray);
        
    $dimensions = array('pageTitle','pagePath','hostname');
    $metrics    = array('pageviews', 'uniquePageviews', 'avgTimeOnPage');   
		
						try{
							$optParams = array(
									'dimensions' => 'ga:pageTitle,ga:pagePath,ga:hostname',
									'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',      
									'filters' => $filterString,
									'max-results' => '1000');

							$gaResults = $analytics->data_ga->get(
									'ga:'.wbStatsGAViewId,
									$rangeFrom,
									$rangeTo,
									'ga:sessions',
									$optParams);
							//echo '<!-- $gaResults is '.print_r($gaResults, true).' -->';
						} catch (Exception $ex) {
							//echo '<!-- $ex is '.print_r($ex, true).' -->';
						}		
    
						foreach($allPosts as $currentPost){
							$wb_current_post_name = $currentPost['post_name'];
							$wb_current_post_name_length = strlen($wb_current_post_name);
							
							foreach($gaResults->rows as $result){
								$slugNoSlash = str_replace(array('/update_ga/','/'), '', $result[1]);
								$wb_result_postname = substr($slugNoSlash,0,$wb_current_post_name_length);
								echo "\r\n".'<!- $wb_current_post_name is -'.$wb_current_post_name.'- $wb_result_postname is -'.$wb_result_postname.'- -->';
								if ( $wb_current_post_name == $wb_result_postname){
									$postsBySlug[$wb_current_post_name]['pageViews'] = $postsBySlug[$wb_current_post_name]['pageViews'] + $result[3];
									$postsBySlug[$wb_current_post_name]['uniquePageViews'] = $postsBySlug[$wb_current_post_name]['uniquePageViews'] + $result[4];
									$postsBySlug[$wb_current_post_name]['aveTimeOnPage'] = $postsBySlug[$wb_current_post_name]['aveTimeOnPage'] + ( $result[5] *  $result[3] );
									$postsBySlug[$wb_current_post_name]['wbctr']++;
								}
							
							}

						}
						
    if( !$hasError ){
    ?>
    <br />
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12">                
                <table class="table table-striped table-bordered table-hover" id="gaResults">
                    <thead>
                    <tr>
                        <th class="text-center">Date Published</th>
                        <th class="text-center">Post Title</th>
                        <th class="text-center">Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The total number of pages viewed."></span></th>
                        <th class="text-center">Unique Page Views <span aria-hidden="true" class="glyphicon glyphicon-question-sign video-report-qtip" title="The pageviews that are generated by the same user during the same session."></span></th>
                        <th class="text-center">Average Time On Page</th>
                    </tr>
                    </thead>                
        <?php                
        if( count($allPosts) > 0 ){            
            echo '<tbody>';
            foreach($postsBySlug as $currentPost){
            	
            	if ( defined( 'wb_how_to_videos_category' )){
            		if ( in_category ( wb_how_to_videos_category, $currentPost['ID'])){
            			continue;
            		}
            	}
            	
            	if( (!$wb_stats_options['displayPrivateVideos'] || !$wb_stats_options['displayUnlistedVideos']) && in_array($currentPost['ID'], $privateAndUnlistedPostIds)){
            		echo "wb";
            	}else{
	            	echo '<tr>';
	            	echo '<td class="text-center">'.date("F j, Y", strtotime($currentPost['post_date'])).'</td>';
	            	echo '<td><a href="/'. $currentPost['post_name'].'" target="_BLANK">'.$currentPost['post_title'].'</a></td>';
	            	echo '<td class="text-center">'.( ( trim($currentPost['pageViews']) != '' ) ? ($currentPost['pageViews']) : '-' ).'</td>';
	            	echo '<td class="text-center">'.( ( trim($currentPost['uniquePageViews']) != '' ) ? ($currentPost['uniquePageViews']) : '-' ).'</td>';
	            	echo '<td class="text-center">'.( ( trim($currentPost['aveTimeOnPage']) != '' ) ? (sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews']))) : '-' ).'</td>';
	            	echo '</tr>';
					$videoListToCsv[] = array(	date("F j, Y", strtotime($currentPost['post_date'])),
												$currentPost['post_title'],
												"http://".$wb_ent_options[channeldomain]."/".$currentPost['post_name'],
												$currentPost['pageViews'],
												$currentPost['uniquePageViews'],
												sec2hms(($currentPost['aveTimeOnPage']/$currentPost['pageViews']))
											);
            	}
            }      
            echo '</tbody>';
        }
        else{
        ?>
                    <tbody>
                        <tr>
                            <td style="border-right: 0px none;">There are no videos to display.</td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none; border-right: 0px none;"></td>
                            <td style="border-left: 0px none;"></td>
                        </tr>
                    </tbody>
        <?php
        }
        ?>
                </table>
            </div>
        </div>
    </div>
    
    <?php
    }
}

/* Edits By: Dan */
if( $page == 'videoCatKey'  ){ 
	require_once('seach-by-cat-keyword.php');
}

if( count($videoListToCsv) > 1 ){
   $csvFileNamePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/csvfiles/'.$csvFileName;
   if( !file_exists('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/csvfiles/') ){
       mkdir('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/csvfiles/');
   }      
   $csvFileHandler = fopen($csvFileNamePath, 'w'); 
   foreach ($videoListToCsv as $fields) {
            $fields = array_map("utf8_decode", $fields);
       fputcsv($csvFileHandler, $fields);
   }
   fclose($csvFileHandler);   
   
   if( file_exists($csvFileNamePath) ){
   ?>
   <a class="btn btn-primary wb-stat-buttons" href="/csvfiles/<?php echo $csvFileName; ?>" download>Export Results</a>
   <?php 
   }
  
}
?>