    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <script src="<?php echo plugins_url('workerbee-video-stats/js/bootstrap.min.js') ; ?>"></script>
    <link href="<?php echo plugins_url('workerbee-video-stats/css/bootstrap.min.css') ; ?>" rel="stylesheet">
    <script src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css" rel="stylesheet">
    
    <script src="//cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.js" ></script>
    <link href="//cdn.jsdelivr.net/qtip2/2.2.1/jquery.qtip.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css" rel="stylesheet">
    <script type="text/javascript">
	
    $(document).ready(function(){
        $('#bcResults').DataTable({
            "order": [ 0, 'desc' ]
        });
        $('#gaResults').DataTable({
            "order": [ 0, 'desc' ]
        });        
		/*
        $( "#rangeFrom" ).datepicker({
            minDate: new Date(2005, 1 - 1, 1),
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
              $( "#rangeTo" ).datepicker( "option", "minDate", selectedDate );
            }
        });

        $( "#rangeTo" ).datepicker({
            minDate: new Date(2005, 1 - 1, 1),
            changeMonth: true,
            changeYear: true,
            onClose: function( selectedDate ) {
              $( "#rangeFrom" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        */
        //help texts
        //$('.video-report-qtip[title]').qtip();
        jQuery.widget.bridge( 'jQueryUITooltip', jQuery.ui.tooltip );
        $('.video-report-qtip[title]').jQueryUITooltip({
            track: true
         });
        
        /*
        $('#vidCat').change( function() {
            location.href = '/wp-admin/admin.php?page=<?php echo trim($_GET['page']); ?>&display=<?php echo $page; ?>&rangeFrom='+$('#rangeFrom').val()+'&rangeTo='+$('#rangeTo').val()+'&vidCat='+$('#vidCat').val();
        });
        */
    });
    </script>

