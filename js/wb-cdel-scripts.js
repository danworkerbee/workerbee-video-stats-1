function updateNotifyMode(){
   jQuery(function ($) {
      var notifyDays = $('#notifyDays').val();
      if(notifyDays == 'default'){
         $('#notifyMode').val('on');
      }
      else if( parseInt(notifyDays) > 0 && $('#notifyMode').val() == 'on' ){
         $('#notifyMode').val('after');
      }
   });
}

function confirmMediaDelete(){
   jQuery(function ($) {
     if( $('td input:checked').length > 0 ){
        $( "#dialog-confirm-multi" ).show();
        $( "#dialog-confirm-multi" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete Media": function() {
                $( this ).dialog( "close" );
                submitToggler = true;
                
                $('#mediaListForm').submit();
              },
              Cancel: function() {
                $( this ).dialog( "close" );                
              }
            }
          });        
     }
     else{
        $( "#dialog-confirm-single" ).show();
        $( "#dialog-confirm-single" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete Medium": function() {
                $( this ).dialog( "close" );
                submitToggler = true;  
                $('#mediaListForm').submit();
                             
              },
              Cancel: function() {
                $( this ).dialog( "close" );
              }
            }
          });           
     }
   });
   
}


function submitMediaFormProcessor(){
   if(!submitToggler){
      confirmMediaDelete();   
   }   
   return submitToggler;     
}



function toggleCheckboxes(){
   jQuery(function ($) {
      if( $('th input[type="checkbox"]').attr('checked') ){
         $('td input[type="checkbox"]').attr('checked', 'checked');   
      }
      else{
         $('td input[type="checkbox"]').removeAttr('checked');
      }      
   });
}


function submitAmbassadorFormProcessor(){
   var returnVar = true;
   
   jQuery(function ($) {
      if( $('#password').length > 0 && $('#confirmPassword').length > 0 ){
         
         if( $('#password').val().length != 0 && ( $('#password').val().length < 6 || $('#confirmPassword').val().length > 20 ) ){
            returnVar = false;
            $('#passwordDialogMessage').html('Passwords need to be between 6 and 20 characters long.');
            $( "#dialog-check-pass" ).dialog({
               resizable: false,
               height:200,
               modal: true,
               buttons: {
                 Ok: function() {                    
                   $( this ).dialog( "close" );
                 }
               }
             });
             
         }
         
         if( $('#password').val() != $('#confirmPassword').val() ){
            returnVar = false;
            $('#passwordDialogMessage').html('Passwords must match.');
            $( "#dialog-check-pass" ).dialog({
               resizable: false,
               height:200,
               modal: true,
               buttons: {
                 Ok: function() {                    
                   $( this ).dialog( "close" );
                 }
               }
             });
             
         }
         
      }   
   });   

   
   return returnVar;
}



function confirmAmbassadorDelete(){
   jQuery(function ($) {
     if( $('td input:checked').length > 0 ){
        $( "#dialog-confirm-multi" ).show();
        $( "#dialog-confirm-multi" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete": function() {
                $( this ).dialog( "close" );
                submitToggler = true;
                
                $('#ambassadorListForm').submit();
              },
              Cancel: function() {
                $( this ).dialog( "close" );                
              }
            }
          });        
     }
     else{
        $( "#dialog-confirm-single" ).show();
        $( "#dialog-confirm-single" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete": function() {
                $( this ).dialog( "close" );
                submitToggler = true;  
                $('#ambassadorListForm').submit();
                             
              },
              Cancel: function() {
                $( this ).dialog( "close" );
              }
            }
          });           
     }
   });
   
}


function submitAmbassadorDelProcessor(){
   if(!submitToggler){
      confirmAmbassadorDelete();   
   }   
   return submitToggler;     
}


function confirmListDelete(){
   jQuery(function ($) {
     if( $('td input:checked').length > 0 ){
        $( "#dialog-confirm-multi" ).show();
        $( "#dialog-confirm-multi" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete": function() {
                $( this ).dialog( "close" );
                submitToggler = true;
                
                $('#displayListForm').submit();
              },
              Cancel: function() {
                $( this ).dialog( "close" );                
              }
            }
          });        
     }
     else{
        $( "#dialog-confirm-single" ).show();
        $( "#dialog-confirm-single" ).dialog({
            resizable: false,
            height:200,
            modal: true,
            buttons: {
              "Delete": function() {
                $( this ).dialog( "close" );
                submitToggler = true;  
                $('#displayListForm').submit();
                             
              },
              Cancel: function() {
                $( this ).dialog( "close" );
              }
            }
          });           
     }
   });
   
}

function submitListDelProcessor(){
   if(!submitToggler){
      confirmListDelete();   
   }   
   return submitToggler;     
}


